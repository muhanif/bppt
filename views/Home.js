
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Navigator,
  Text,
  ScrollView,
  View,
  TouchableHighlight,
  DrawerLayoutAndroid,
} from 'react-native';

import Usulan from './Usulan';
import TambahUsulan from './TambahUsulan';

function getRouteIndex(routeArr,indexName){
  return routeArr.filter(function(value,index){
    return value.index === indexName;
  }).join("");
}
export default class Home extends Component{

  constructor(props){
    super(props);
    this.state = {
      currentPage : 'Usulan'
    }

    this.routeStack = [
      {
        index : 'Usulan',
        component : Usulan,
        passProps :{
          routeStack : this.routeStack
        }
      },
      {
        index : 'TambahUsulan',
        component : TambahUsulan,
        passProps : {
          routeStack : this.routeStack
        }

      }

    ]
    
    this.initialRoute = {
      index : 'Usulan',
      component : TambahUsulan,
      passProps : {
        routeStack : this.routeStack
      }
    }
  }


  formRenderScene(route,navigator){
    return React.createElement(route.component, { ...this.props, ...route.passProps, route, navigator } )
  }


  render(){
    return(
      <View style={styles.container} >
      <Navigator 
        initialRoute={this.routeStack[0]}
        initialRouteStack={this.routeStack}
        renderScene={this.formRenderScene}
        refs="homeNav"
      />
    </View>
    )
  }

}

const styles=StyleSheet.create({
  container :{
    flex : 1,
    position: 'relative',
    width:null,
    top:0,
    left:0,
    bottom:0,
    right:0,
  },
  floatingButton : {
    position:'absolute',
    right : 10,
    bottom : 10,

  },
  floatingButtonShadow : {
    textShadowColor : "rgba(0,0,0,.2)",
    textShadowRadius : 2,
    textShadowOffset : {width : 1, height: 1}

  },
  
})
