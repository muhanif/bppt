// ES 6 import = module / helper
// ES 5 import = Component / Styling (style)

import React, { Component } from 'react';
import Icon  from 'react-native-vector-icons/EvilIcons';
import {
  StyleSheet,
  Text,
  ScrollView,
  TouchableHighlight,
  DrawerLayoutAndroid,
  View
} from 'react-native';

import { excerpt, DummyData
} from '../helper/DummyHelper.js';

import ListUsulanHeader from '../components/home/ListUsulanHeader';
import UsulanItem from '../components/home/UsulanItem';
import TambahUsulan from '../views/TambahUsulan';


export default class ListProyek extends Component {
  constructor(props){
    super(props);
    //Populate Dummy Data for Scroll Testing
    this.DUMMY_DATA = (() => {
      result = [];
      for (let i = 0;  i < 5 ; i++) {
        result.push(new DummyData());
      }
      return result;
    })();
    //

    this.state = {
      currentListPage : 0,
    }
  }

  handleAddButtonPress(){
    this.props.navigator.push({
      index : 'TambahUsulan',
      component : TambahUsulan,
      passProps : {
        cancelForm : this.cancelForm.bind(this)
      }
    });
  }
  cancelForm(){
    this.props.navigator.popToTop();
  }

  handleHeaderPress(){
    let newListPage;
    if (this.state.currentListPage === 0)
      newListPage = 1;
    else
      newListPage = 0;
    this.setState({currentListPage : newListPage })
  }


  render() {
    return (
      <View style={styles.container} >
      <ListUsulanHeader 
        handleHeaderPress={this.handleHeaderPress.bind(this)}
        currentListPage={this.state.currentListPage}
        ref={'HeaderList'}
      />
      <ScrollView style={styles.usulanList} >
        {this.DUMMY_DATA.map((content) => (
          <UsulanItem
            titleText={content.titleText}
            imageUrl={content.imageUrl}
            sentDate={content.sentDate}
            locationString={content.locationString}
            descriptionText={content.descriptionText}
            status={content.status}
          />
        ))}
      </ScrollView>
      <TouchableHighlight 
        style = {styles.floatingButton} 
        onPress={this.handleAddButtonPress.bind(this)}

      >
        <Text style={styles.floatingButtonShadow}>
          <Icon name="plus" size={70} color={"#ecf0f1"} />
        </Text>
      </TouchableHighlight>

      </View>	
    );
  }
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#FFFFFF',
     },
    usulanList :{
        //TODO
        paddingVertical : 10,
    },
  floatingButton : {
    position:'absolute',
    right : 10,
    bottom : 10,

  },
  floatingButtonShadow : {
    textShadowColor : "rgba(0,0,0,.2)",
    textShadowRadius : 2,
    textShadowOffset : {width : 1, height: 1}

  },

})
