import React, { Component } from 'react';
import Icon  from 'react-native-vector-icons/EvilIcons';
import {
  AppRegistry,
  Navigator,
  ScrollView,
  DrawerLayoutAndroid,
} from 'react-native';

import FormAddPost from '../components/form/FormAddPost';
import FormGetPhoto from '../components/form/FormGetPhoto';
import FormAddLocation from '../components/form/FormAddLocation'
import styles from '../styles';
import FormNavigation from '../components/form/FormNavigation';
import FormConfirm from '../components/form/FormConfirm';


export default class TambahUsulan extends Component{
  
  constructor(props){
    super(props);
    this.state = {
      currentIndex : 0,
    }

    this.routeStack = [
      {
        index : 'Add Post',
        component : FormAddPost
      },
      {
        index : 'Add Photo',
        component : FormGetPhoto
      },
      {
        index : 'Add Location',
        component : FormAddLocation,
      },
      {
        index : 'Confirm',
        component : FormConfirm
      }
    ]

    this.initialRoute = {
      index :0,
      component : FormAddPost
    }
  }
  
  

  formRenderScene(route,navigator){
  return React.createElement(route.component, { ...this.props, ...route.passProps, route, navigator } )
  }

  
  render(){
    return(
      <Navigator
        initialRoute={this.routeStack[0]}
        initialRouteStack={this.routeStack}
        renderScene={this.formRenderScene}
        navigationBar={
          <FormNavigation 
            routeStack={this.routeStack} 
            cancelForm={this.props.cancelForm}
          />}
      />
  )
  }


}
