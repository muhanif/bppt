function createDetailInfo(){
  /* //TODO
   *
   * Return :
   * arrObj : []
   * Detail data structure
   *
   * Detail Data = {
   *          rekanan : {
   *            (string) namaRekanan :
   *            (string) alamatRekanan :
   *            (string ) skpd : 
   *
   *          }
   *          informasi : {
   *            (date String)tanggalMulai : 
   *            (date String) targetSelesai:
   *            (string) anggaran :
   *            (string) tahun anggaran : 
   *            (string) status : 
   *            (location String) lokasi :
   *            (location Coordinate) : 
   *          }
   *          laporanProgress : [
   *            (obj) detailLaporan1 ,
   *            (obj) detailLaporan2 ,
   *            .......
   *          ]
   *
   *          }
   *
   */
  const DATA = {
    rekanan : {
      "Nama Rekanan" : "PT Maju Bersama",
      "Alamat Rekanan" : "Jalan X nomer 1 Bandung",
      "SKPD" : "Dinas Pekerjaan Umum"
    },
    informasi: {
      "Tanggal Mulai" : "25 Maret 2015",
      "Target Selesai" : "20 Januari 2016",
      "Anggaran" : "13.000.000.000",
      "Tahun Anggaran" : "2016",
      "Status" : "Pengerjaan",
      "Lokasi" : "Dipatiukur Coblong, BDG"
    },
  }

  return DATA

}

// ES 6 import = module / helper
// ES 5 import = Component / Styling (style)

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  TouchableHighlight,
  Navigator,
  DrawerLayoutAndroid,
  View
} from 'react-native';

import { excerpt, ProyekListDummyData
} from '../helper/DummyHelper';
import UsulanItem from '../components/home/UsulanItem';
import ProyekDetail from './proyek/ProyekDetail'
import styles from '../styles';




export class ListProyek extends Component {

  constructor(props){
    super(props);
    

    //Populate Dummy Data for Scroll Testing
    this.DUMMY_DATA = (() => {
      result = [];
      for (let i = 0;  i < 5 ; i++) {
        result.push(new ProyekListDummyData());
      }
      return result;
    })();
    

    
    this.state = {
      isSearching : false,
    }
  }


  render() {
    return (
      <View style={styles.container} >
        <ScrollView style={styles.usulanList} >
          {this.DUMMY_DATA.map((content) => (
            <UsulanItem
              imageUrl={content.imageUrl}
              caller="ListProyek"
              sentDate={content.sentDate}
              titleText={content.titleText}
              locationString={content.locationString}
              descriptionText={content.descriptionText}
              status={content.status}
              onPress={this.props.onLinkPress}
            />
          ))}
        </ScrollView>
      </View>	
    );
  }


}
export default class Proyek extends Component {


  //TODO 

  constructor(props){
    super(props);

    this.state={
      initialRoute : 0
    }

    this.DATA = createDetailInfo()

    this.routeStack=[
      {
        index : 'ListProyek',
        component : ListProyek,
        passProps : {
          routeStack : this.routeStackProyekNav,
          onLinkPress: this.onLinkPress.bind(this)
        }

      },
      {
        index : 'ProyekDetail',
        component : ProyekDetail,
        passProps : {
          routeStack : this.routeStack,
          detailData : this.DATA,
        } 
      }
    ]

  }


  renderScene(route,navigator){
    return React.createElement(route.component, { ...this.props, ...route.passProps, route, navigator } )
  }


  onLinkPress(){
    this.props.navigator.push(this.routeStack[1])

  }


  render(){
    return(
      <Navigator 
        initialRoute={this.routeStack[this.state.initialRoute]}
        initialRouteStack={this.routeStack}
        renderScene={this.renderScene}
        ref={'ProyekNav'}
      />

    )
  }

}

