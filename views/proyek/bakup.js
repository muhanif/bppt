import React, { Component } from 'react';
import Icon  from 'react-native-vector-icons/EvilIcons';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ScrollView,
  TouchableHighlight,
  TextInput,
  Dimensions
} from 'react-native';




class FullWidthImage extends Component {


  constructor(props){
    super(props);
  }


  componentDidMount(){

  }

  render(){
    return(
      <View style={styles.imageWithTextContainer}>
        <View style={styles.imageContainer} >
          <Image 
            source={require('../../dummy_image/proyek/proyek-1.png')}
            style={styles.image}
          />
          <View style={styles.textContainer} >
            <Text style={styles.text}> Proyek X </Text>
            <Text style={styles.textChild}> 25 Maret 2015 - 20 Januari 2016 </Text>
          </View>
        </View>
      </View>
    )
  }

}

export default class ProyekDetail extends Component {

  constructor(props){
    super(props);
    this.state={
      modalOverlay : null
    }
  }

  componentDidMount(){
    this.setState({
      modalOverlay : false
    })
  }

  toggleModal(){
    this.setState({
      modalOverlay : !this.state.modalOverlay
    })
  }


  render(){
    return(
      <View style={styles.container}>
        <Notifier />
        <ScrollView style={styles.scrollView}>
          <FullWidthImage />
          <Galery onPress={this.toggleModal} />
          <ItemDetail {...this.props}/>
          <CommentBox />
          <OverlayModal opened={this.state.modalOverlay} />
        </ScrollView>
      </View>
    )
  }
}

class Notifier extends Component {

  constructor(props){
    super(props);
    this.state = {
      closed : null
    }
  }

  componentDidMount(){
    this.setState({
      close : false
    })
  }

  closeButtonPress(){
    this.setState({
      closed : !this.state.closed
    })
  }

  render(){
    return(
      <View 
        style={this.state.closed ? styles.notifierContainerClosed :
          styles.notifierContainer}
        >
          <View style={styles.notifierTitleContainer}>
            <Text style={styles.notifierTitleText}>
              Bagaimana Tanggaan Anda Tentang Proyek Ini ?
            </Text>
          </View>
          <View style={styles.notifierControlContainer}>
            <Text onPress={this.closeButtonPress.bind(this)}>Suka</Text>
            <Text onPress={this.closeButtonPress.bind(this)}>Tidak Suka</Text>
          </View>
          <TouchableHighlight style={styles.notifierClose} onPress={this.closeButtonPress.bind(this)}>
            <Icon name={"close"} size={22} color="#88a" />
          </TouchableHighlight>
        </View>
    )
  }
}

class Galery extends Component{
  constructor(props){
    super(props);
  }

  render(){
    return(
      <ScrollView horizontal={true} style={styles.galeryContainer}>
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-1.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-2.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-3.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-4.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-5.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-2.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-1.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-3.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-4.png')} />
        <Image
          style={styles.galeryThumbnail}
          source={require('../../dummy_image/proyek/thumbnail/proyek-1.png')} />
      </ScrollView>
    )
  }
}

class ItemDetail extends Component {

  constructor(props){
    super(props);
    this.state = {
      folded : true,
    }
  }


  componentWillMount(){
    const {detailData} = this.props
    this.detailData = detailData
    this.rekanan = detailData.rekanan
    this.informasi = detailData.informasi
    this.dataKeys = Object.keys(detailData)
  }


  foldPressed(){
    this.setState({
      folded : !this.state.folded,
    })
  }

  render(){
    return(
      <View 
        style={
          this.state.folded ?
          [styles.itemDetailContainer, styles.itemDetailFolded] :
          [styles.itemDetailContainer, styles.itemDetailOpen]
        } 
      >
        <View style={styles.itemDetailControl} >
          <View>
            <Text onPress={this.foldPressed.bind(this)} style={styles.foldButton} > 
              <Icon name="chevron-down" size={16}  color={"#444"} />
              See Detail 
            </Text>
          </View>

          <View style={styles.itemDetailLike}>
            <View>
              <Text>
                25<Icon 
                  name="chevron-up"
                  style={{transform : [{rotate : '130 deg'}] }}  
                  size={32} 
                  color={"#5d5"} /> 
              </Text>
            </View>
            <View>
              <Text>3<Icon name="chevron-down" size={32} color={"#d55"} /> </Text>
            </View>
          </View>
        </View>
        {!this.state.folded ?
          <View style={styles.itemDetailContent}>
            {
              //iterate through detailData keys to produce header
              this.dataKeys.map( (header) =>  {
                return (
                  <View style={styles.detailBox}>
                    <Text style={styles.detailHeader}>{header.toUpperCase()}</Text>
                    {
                      //iterate through detaildata.header obj to get child and content
                      Object.keys(this.detailData[header]).map( (content) => {
                        return (
                          <Text style={styles.detailContent}> 
                            {content} : {this.detailData[header][content]}
                          </Text>
                        )
                      })
                    }
                  </View>
                )
              })
            }
            <View style={styles.detailBox}>
              <Text style={styles.detailHeader}>Lokasi </Text>
              <View>
                <Image
                  source={require('../../dummy_image/proyek/navi.png')}
                  style={styles.detailMaps}
                />
              </View>
              <Text>20 Menit dari lokasi anda </Text>
            </View>
          </View>
          :
          <View/>
        }

      </View>
    )
  }
}

class CommentBox extends Component{
  render(){
    return(
      <View style={styles.commentBox}>
      <Text style={styles.commentHeader}>Komentar</Text>
        <Comment uname={"Bob"} comment={"Petugas Belum Datang Sejak Bulan lalu"}/>
        <Comment uname={"Petugas 1"} comment={"Terimakasih Laporanya Pak. Akan Segera Kami Tindaklanjuti"}/>
        <CommentForm />
      </View>
  )
  }
}

class Comment extends Component {
  render(){
    return (
      <View style={styles.comment}>
        <Image
          style={styles.profilePicture}
          source={require('../../dummy_image/avatar.png')}
        />
        <View style={styles.userComment}>
          <Text>{this.props.uname}</Text>
          <Text>{this.props.comment}</Text>
        </View>
      </View>
      
  )
  }
}

class CommentForm extends Component{
  render(){
    return (
      <View style={styles.comment}>
        <Image
          style={styles.profilePicture}
          source={require('../../dummy_image/avatar.png')}
        />
        <View style={styles.userComment}>
          <Text>{this.props.uname}</Text>
          <TextInput
            style = {styles.formBox}
            multiline={true}
          />
        </View>
      </View>
        
  )
  }
}

class OverlayModal extends Component{
  constructor(props){
    super(props);
    this.state={
      opened : false,
    }
  }

  componentWillReceiveProps(props){
    const { opened } = props
    this.setState({
      opened : opened
    })
  }

  
  render(){
    return (
      <View 
        style={
          this.state.opened ?
          [styles.overlayContainer,styles.displayed] :
          [styles.overlayContainer,styles.hidden]
        }
      >
        <Text style={styles.overlayTitle}>Judul Gambar </Text>
        <FullWidthImage />
        <CommentBox />
      </View>
  )
  }
}
const styles = StyleSheet.create({
  container: {
    flex : 1,

  },
  scrollView: {
    height : null,
    backgroundColor: '#333',
  },
  imageWithTextContainer:{
  },
  notifierContainer: {
    position: 'absolute',
    paddingHorizontal : 25,
    paddingVertical : 5,
    top: 0,
    left:0,
    right:0,
    backgroundColor: "#fff",
    flex : 1,
    flexDirection : 'column',
    alignItems : 'stretch',
    justifyContent : 'flex-start',
    zIndex : 1000
  },
  notifierContainerClosed:{
    height : 0,
  },
  notifierTitleContainer:{
  },
  notifierControlContainer:{
    paddingHorizontal : 10,
    marginLeft : 3,
    flexDirection: 'row',
    justifyContent : 'space-between',
    alignItems : 'center'
  },
  notifierTitleText:{
    textAlign: 'center',
    lineHeight : 28,
  },
  notifierClose:{
    position: 'absolute',
    top:2,
    right:3,
  },
  imageContainer :{
    width : Dimensions.get('window').width,
    height : 200,
  },
  image : {
    position : 'absolute',
    top: 0,
    bottom : 0,
    left : 0,
    right: 0,
    width:null,
    height:null
  },
  textContainer: {
    flex : 1,
    position: 'absolute', 
    bottom : 0,
    left : 0,
    right : 0,
    width : null,
    backgroundColor : "rgba(0,0,0,.6)",
    paddingHorizontal : 5,
    paddingVertical : 10,
    borderBottomColor : 'rgba(0,0,0,.2)',
    borderBottomWidth : 7,
  },
  text:{
    fontSize : 20,
    fontWeight : '800',
    color : '#eff',
    textShadowColor : "rgba(0,0,0,.3)",
    textShadowRadius : 1,
    textShadowOffset : {width: 1, height: 1}
  },
  textChild:{
    fontSize : 12,
    color : "#eff",
  },
  galeryContainer : {
    width : Dimensions.get('window').width,
    height: 95,
    backgroundColor : '#ddd',
  },
  galeryThumbnail :{
    width: 75,
    height: 75,
    alignSelf : 'center',
    borderColor: 'rgba(255,255,255,0.9)',
    borderWidth: 2,
    marginHorizontal: 15,
    borderRadius: 3,
  },
  itemDetailContainer : {
    width: Dimensions.get('window').width,
    marginVertical : 20,
    backgroundColor : "#fff",
  },
  
  itemDetailOpen : {
    height : Dimensions.get('window').height,
    height : null
  },
  itemDetailFolded : {
    height : 30,
  },
  itemDetailControl: {
    flexDirection: 'row',
    justifyContent : 'space-between',
    alignItems : 'center',
  },
  itemDetailLike : {
    flex : 1,
    flexDirection : 'row',
    justifyContent : 'flex-end',
    alignItems : 'flex-start',
    position: 'absolute',
    right : 10,
  },
  foldButton : {
  },
  itemDetailContent : {
    flex : 1,
    justifyContent: 'space-between',
    paddingHorizontal: 9,
    alignItems : 'stretch',
    paddingVertical : 3,
  },
  detailBox :{
    marginVertical : 10,
  },
  detailHeader:{
    fontWeight : '800',
  },
  detailMaps :{
    width : 300,
    height: 150,
  },
  commentBox : {
    backgroundColor:"#fff",
    minHeight: (Dimensions.get('window').height / 3),
    padding : 7,
    paddingBottom: 40,
  },
  commentHeader : {
    fontWeight: '600',
    fontSize: 22,
  },
  comment : {
    flex : 1,
    flexDirection: 'row',
    justifyContent : 'flex-start',
    alignItems : 'flex-start',
    paddingTop : 20,
    borderBottomColor: "#cdf",
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  profilePicture: {
    height:50,
    width:50,
    marginHorizontal : 5,
  },
  userComment: {
    flex:1,
    alignItems: 'stretch',
    justifyContent : 'center'
  },
  formBox : {
    flex : 1,
    marginBottom : 5,
    alignItems : 'stretch',
    textDecorationLine : "none",
    borderColor : "#bcd",
    borderWidth : 1,
  },
  overlayContainer:{
    flex:1,
    position : 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    width:null,
    height:null,
    backgroundColor: "rgba(0,0,0,0.7)"
  },
  displayed : {
    opacity : 1
  },
  hidden : {
    opacity : 0
  },
  overlayTitle : {
    width : Dimensions.get('window').width,
    fontSize : 21,
    fontWeight : '400'
  }
  
})
