/**
 * DUMMY DATA BLOCK
 * TESTING PURPOSE
 */
//Testing text variant length
const normalText = `Kerusakan Parah Jembatan Penghubung di.Kampung X`;
const longerText = `Kerusakan parah trotoar di jalan x kota
y menyebabkan terganggunya pejalan kaki di sekitar jalan`;
const muchLongerText = `KKerusakan Parah Jembatan Penghubung di Kampung XKerusakan Parah Jembatan Penghubung di Kampung Xerusakan parah trotoar di jalan x kota y menyebabkan terganggunya pejalan kaki di sekitar jalan serta Kerusakan Parah Jembatan Penghubung diKampung X`;

function getRandomImageUrl(){
  let index = Math.floor(Math.random() * 6 );
  return '../dummy_image/rusak-'+index+'.png';
}
function getRandomText(arr){
  let index = Math.floor(Math.random() * arr.length );
  return arr[index];
}

//helper function
export function excerpt(text){
  return text.length > 140 ? 
    text.substring(0,140)+"..." :
    text
}

//fungsi untuk menggenerate dummy data
//dynamic image file error
//for later troubleshoot:
//might be because file size (and react-native only support png)
export function DummyData(){
  this.titleText = getRandomText([
    "Kerusakan Parah Jembatan Desa Y",
    "Kerusakan Jalan Kota X",
    "Kerusakan Halte di daerah Z"
  ]);
  this.imageUrl = getRandomImageUrl;
  this.sentDate = getRandomText([
    '5 Oktober 2016',
    '3 Mei 2016',
    '7 Januari 2016',
    '10 Desember 2015']);
  this.locationString = getRandomText([
    'Dipatiukur, Coblong, BDG',
    'Desa 1 , Kecamatan 2 , BDG',
    'Desa X , Kecamatan Y , BDG',
    'Desa 3 , Kecamatan XYZ , BDG',
    'Desa , Kecamatan , BDG'
  ]);
  this.descriptionText = excerpt(getRandomText(
    [normalText,longerText,muchLongerText]
  ));
  this.status = getRandomText([
    'pengerjaan',
    'usulan ditolak',
    'data tidak valid',
    'Sedang divalidasi'
  ]);

}

export function ProyekListDummyData(){
  this.titleText = getRandomText([
    'Pembangunan Gedung Kota',
    'Perbaikan Jalan X',
    'Perbaikan Jembatan Y'
])
  this.imageUrl = getRandomImageUrl;
  this.sentDate = getRandomText([
    '3 Mei 2015',
    '5 Oktober 2014',
    '1 Januari 2016',
    '20 Oktober 2017'
  ])
  this.descriptionText= getRandomText([
    'Jembatan Penghubung Antara Desa X dan Y',
    'Perbaikan Jalan di Kota X',
    'Pembangunan Gedung Instansi X'
]) 
  this.status = getRandomText([
    'Pengerjaan',
    'Selesai',
    'Perencanaan'
])

}
/*
 * END OF DATA TESTING BLOCK
 */
