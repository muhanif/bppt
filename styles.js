const React = require('react-native');
const { StyleSheet } = React;

/*
 * STYLE TODO
 * Make styles reusable instead
 * of unique style in every components
 */

const styles = StyleSheet.create({
  container:{
    flex:1,
    backgroundColor:'#FFFFFF',
  },
  navbarWrapper:{
    paddingTop: 15,
    height: 45,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
    borderColor : "#bbf",
    borderBottomWidth : StyleSheet.hairlineWidth
  },
  searchBox:{
    flexDirection: 'row',
    height : 30,
    justifyContent: 'center',
    flex : 2
  },
  topNavbar:{
    backgroundColor: '#FDFDFF',
    paddingHorizontal: 12,
  },
  navbarTitle:{
    fontWeight: '600',
    fontSize: 17,
  },
  bottomNavbar:{
    paddingHorizontal: 30,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#DDDDDD',
  },
  bottomNavbarContent : {
    fontSize: 16,
    paddingBottom : 6,
    paddingHorizontal :8,
  },
  bottomNavbarSelected: {
    color : '#3498db',
    fontWeight: '600',
    borderBottomWidth: 1,
    borderBottomColor: '#1abc9c',
  },
  bottomNavbarDisabled: {
    color : '#557',
    fontWeight: '200',
  },
});

export default styles;
