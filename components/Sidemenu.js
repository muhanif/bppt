'use strict';
import React,{ Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import ReactNative,{
  StyleSheet,
  Image,
  Text,
  View
} from 'react-native';


export default class Sidemenu extends Component {
  render(){
    return(
      <View>
        <SidemenuTop {...this.props} />
        <SidemenuBottom 
        linkListener={this.props.linkListener}
        />
      </View>
    )
  }
}

class SidemenuTop extends Component {
  render(){
    return(
      <View style={styles.sidemenuTop}>
        <Text>Side Menu </Text>
        <Image 
          style={styles.sidemenuTopBackground} 
          source={require('../dummy_image/cover.png')}
        />
        <SidemenuAvatar />
        <Text style={styles.sidemenuUsername}>
          Nama User
        </Text>
      </View>
    )
  }
}

class SidemenuAvatar extends Component{

  render(){
    return (
      <View>
        <Image
          style={styles.sidemenuAvatar}
          source={require('../dummy_image/avatar.png')}
        />
      </View>
    )
  }
}


class SidemenuBottom extends Component {

  constructor(props){
    super(props);
    this.state = {
      clicked : 0
    }
  }

  linkPressed(n){
    this.props.linkListener(n)
    this.setState({
      clicked : n
    })
  }

  onComponentDidUpdate(){
  }  

  render(){
    return(
      <View style={styles.sidemenuBottom}>
        <Text 
          style={this.state.clicked == 0 ? styles.linkSelected : styles.boldLink}
          onPress={this.linkPressed.bind(this,0)}
        >
          Daftar Usulan
        </Text>
        <Text 
          style={this.state.clicked == 1 ? styles.linkSelected : styles.boldLink}
          onPress={this.linkPressed.bind(this,1)}
        >
          Proyek Di Sekitar
        </Text>
        <Text style={styles.boldLink}>
          Log Out
        </Text>
      </View>
    )
  }
}

const styles=StyleSheet.create({
  sidemenuWrapper:{
    //TODO
    flex : 1,
    backgroundColor: "#fff",
    borderRightWidth : StyleSheet.hairlineWidth,
    borderRightColor : "#AAAADD"
  },
  sidemenuTop:{
    //TODO
    flex : 1,
    height:145,
    borderBottomWidth : StyleSheet.hairlineWidth,
    borderBottomColor : "#bbf",
    padding : 10,

  },
  sidemenuTopBackground:{
    position : 'absolute',
    top : 0,
    bottom: 0,
    left:0,
    right:0,
    width: null,
    height: null,

  },
  sidemenuAvatar:{
    width: 60,
    height: 60,
    borderRadius:30,
    borderColor : "#fff",
    borderWidth : 1

  },
  sidemenuUsername:{
    //TODO
    marginLeft : 5,
    color : "#eef",
    fontWeight: "400",
    fontSize : 18,
    textShadowColor : "rgba(5,5,5,.6)",
    textShadowRadius : 3,
    textShadowOffset : {width : 1, height: 1}
  },
  sidemenuBottom:{
    //TODO
    padding: 10,
  },
  boldLink:{
    fontWeight : '600',
    margin : 4,
    fontSize : 15
  },
  linkSelected:{
    fontWeight : '600',
    margin : 4,
    fontSize : 15,
    color : '#e67e22'
  },

})
