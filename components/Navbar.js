'use strict';
import React,{ Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import { 
  Stylesheet,
  Text,
  TextInput,
  View,
  DrawerLayoutAndroid,
  TouchableHighlight, 
} from 'react-native';
import Sidemenu from './Sidemenu';
import styles from '../styles';

class TitleText extends Component{
  render(){
    return (
      <View>
        <Text style={styles.navbarTitle}>
          {this.props.titleText}
        </Text>
      </View>
    )

  }
}
class SearchBox extends Component{
  constructor(props){
    super(props);
    this.state = { text : ' ' }
  }

  render(){
    return (
      <View style={styles.searchBox}>
        <Icon name="search" size={28} color="#222"/>
        <TextInput
          style={{height : 30, flex : 2}}
          onChangeText = {(text) => this.setState({text})}
          value={this.state.text}
        />
      </View>
    )
  }
}

export default class Navbar extends Component {


  constructor(props){
    super(props);
    this.state = { isSearching : false }

  }


  handleSearchPress(){
    let newIsSearchingState = !this.state.isSearching
    this.setState({isSearching : newIsSearchingState})
  }


  render(){
    const searchIcon = this.state.isSearching ? "close" : "search";
    const {titleText} = this.props


    return (
        <View style={[styles.navbarWrapper,styles.topNavbar]}>
          <TouchableHighlight onPress={this.props.onPress}> 
            {
              this.state.isSearching ?
              <View style={{width : 0}} /> :
              <Icon name="navicon" size={28} color="#222"/>
            }
          </TouchableHighlight>
          {
            this.state.isSearching ?
            <SearchBox /> :
            <TitleText titleText={titleText} />
          }
          <TouchableHighlight onPress={this.handleSearchPress.bind(this)}> 
            <Icon name={searchIcon} size={24} color="#222" />
          </TouchableHighlight>
        </View>
    )


  }
}


