'use strict';
import React,{ Component } from 'react';
import ReactNative,
{ Image,
  StyleSheet,
  View,
  TouchableOpacity,
  Text
} from 'react-native';
import {excerpt} from '../../helper/DummyHelper';

//Property caller is a dummy property

export default class UsulanItem extends Component{
  render(){
    let titleText = this.props.titleText;
    let sentDate = "Dikirim : " + this.props.sentDate;
    let locationString = this.props.locationString;
    let descriptionText = excerpt(this.props.descriptionText);
    let status = "Status : " + this.props.status;
    //NOT USED, react-native not support dynamic
    //image from local
    let imageUrl = this.props.imageUrl();
    return (
      <TouchableOpacity 
        style={styles.container}
        onPress={this.props.onPress}
        {...this.props}
      >
        <View style={styles.imageContainer}>
        {this.props.caller === "ListProyek" ?
        <Image 
          style={styles.image}
          source={require('../../dummy_image/proyek-1.png')}
        /> 
        : <Image
          style={styles.image}
          source={require('../../dummy_image/rusak-1.png')}
        />}
        <Text style={styles.text}> 
          {titleText}
        </Text>
      </View>
        <View style={styles.textContainer}>
          <Text style={styles.textHeader}>
            {sentDate}
          </Text>
          <Text style={styles.textHeader}>
            {locationString}
          </Text>
          <Text style={styles.textDescription}>
            {descriptionText}
          </Text>
          <Text style={styles.textStatus}>
            {status}
          </Text>
        </View>
      </TouchableOpacity>            
    )
  }
}

const styles=StyleSheet.create({
  container: {
    flex : 1,
    height: 350,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#bbc',
    backgroundColor: '#fafaff',
    alignItems : 'stretch',
    justifyContent : 'flex-start',
    marginHorizontal : 10,
    marginVertical : 10,

  },
  imageContainer :{
    flex: 1,
    minHeight: 200,
  },
  image : {
    position : 'absolute',
    top: 0,
    bottom : 0,
    left : 0,
    right: 0,
    width:null,
    height:null
  },
  textContainer: {
    flex : 1,
    padding : 10,
  },
  textHeader :{
    fontWeight : '600',
    color: '#778',
  },
  text:{
    position: 'absolute',
    bottom : 15,
    left : 5,
    fontSize : 20,
    fontWeight : '800',
    color : '#eff',
    textShadowColor : "rgba(0,0,0,.3)",
    textShadowRadius : 1,
    textShadowOffset : {width: 1, height: 1}
  },
  textDescription:{
  //TODO
  },
  textStatus:{
    color : "#f39c12"
  } 

})
