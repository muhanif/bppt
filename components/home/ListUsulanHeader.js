'use strict';
import React,{ Component } from 'react';
import ReactNative,{
  TouchableHighlight,
  View,
  Text,
  StyleSheet,
} from 'react-native';


export default class ListUsulanHeader extends Component{
  constructor(props){
    super(props);
    this.state = { currentPage : 0 }
  }
  render(){

    // currentPage 0 = "usulan saya";
    // currentPage 0 = "Semua Usulan"
    let currentPage = this.props.currentListPage;
    return(
      <View style={[styles.navbarWrapper,styles.bottomNavbar]}>
        <TouchableHighlight 
          style={[styles.bottomNavbarContent,
            currentPage == 0 ? 
            styles.bottomNavbarSelected :
            styles.bottomNavbarDisabled
          ]}
          onPress={this.props.handleHeaderPress}
        > 
          <Text>Usulanku</Text>
        </TouchableHighlight>
        <TouchableHighlight 
          style={[styles.bottomNavbarContent,
            currentPage == 1 ?
            styles.bottomNavbarSelected :
            styles.bottomNavbarDisabled
          ]}
          onPress={this.props.handleHeaderPress}
        > 
          <Text>Semua Usulan </Text>
        </TouchableHighlight>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  navbarWrapper:{
    paddingTop: 15,
    height: 45,
    flexDirection: 'row',
    alignItems: 'stretch',
    justifyContent: 'space-between',
  },
  searchBox:{
    flexDirection: 'row',
    height : 30,
    justifyContent: 'center',
    flex : 2
  },
  topNavbar:{
    backgroundColor: '#FDFDFF',
    paddingHorizontal: 12,
  },
  navbarTitle:{
    fontWeight: '600',
    fontSize: 17,
  },
  bottomNavbar:{
    paddingHorizontal: 30,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#DDDDDD',
  },
    bottomNavbarContent : {
        fontSize: 16,
        paddingBottom : 6,
        paddingHorizontal :8,
    },
    bottomNavbarSelected: {
        color : '#3498db',
        fontWeight: '600',
        borderBottomWidth: 1,
        borderBottomColor: '#1abc9c',
    },
    bottomNavbarDisabled: {
        color : '#557',
        fontWeight: '200',
    },

})
