
import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
} from 'react-native';


export default class ImageWithText extends Component {


  constructor(props){
    super(props);
  }

  render(){
    return(
      <View style={styles.container}>
        <View style={styles.imageContainer} >
          <Image 
            source={require('../dummy_image/rusak-0.png')}
            style={styles.image}
          />
          <Text style={styles.text}> Mohon Perbaikan Jembatan </Text>
        </View>
        <View style={styles.textContainer}>
          <Text style={styles.textHeader}>Dikirim 12 Maret 2016 </Text>
          <Text style={styles.textHeader}>Duri Kepa, Kebon Jeruk, Jakbar </Text>
          <Text style={styles.textDescripption}>Kerusakan Sangat Parah Menghambat Aktivitas Warga </Text>
        </View>
      </View>
    )
  }

}


const styles = StyleSheet.create({
  container: {
    flex : 1,
    height: 250,
    borderWidth: 1,
    borderRadius: 1,
    borderColor: '#bbc',
    backgroundColor: '#fafaff',
    alignItems : 'stretch',
    justifyContent : 'flex-start',
    marginHorizontal : 10,
    marginVertical : 10,

  },
  imageContainer :{
    width : 300,
    height : 150,
  },
  image : {
    position : 'absolute',
    top: 0,
    bottom : 0,
    left : 0,
    right: 0,
    width:null,
    height:null
  },
  textContainer: {
    flex : 1,
    padding : 10,
  },
  textHeader :{
    fontWeight : '600',
    color: '#778',
  },
  text:{
    position: 'absolute',
    bottom : 15,
    left : 5,
    fontSize : 20,
    fontWeight : '800',
    color : '#eff',
    textShadowColor : "rgba(0,0,0,.3)",
    textShadowRadius : 1,
    textShadowOffset : {width: 1, height: 1}
  },
  textDescription:{
  //TODO
  } 
})
