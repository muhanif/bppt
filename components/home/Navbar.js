'use strict';
import React,{ Component } from 'react';
import Icon from 'react-native-vector-icons/EvilIcons';
import { 
        Stylesheet,
        Text,
        TextInput,
        View,
        TouchableHighlight, 
} from 'react-native';

const styles = require('../../styles.js');
class TitleText extends Component{
    render(){
    return (
        <View>
        <Text style={styles.navbarTitle}>
            {this.props.titleText}
        </Text>
    </View>
    )

    }
}
class SearchBox extends Component{
    constructor(props){
        super(props);
        this.state = { text : 'put your text here' }
    }

    render(){
        return (
            <View style={styles.searchBox}>
                <Icon name="search" size={28} color="#222"/>
                <TextInput
                    style={{height : 30, flex : 2}}
                    onChangeText = {(text) => this.setState({text})}
                    value={this.state.text}
                />
            </View>
    )
    }
}

class Navbar extends Component {
    constructor(props){
        super(props);
        
    }
    render(){
        let searchIcon = this.props.isSearching ? "close" : "search";
        let tittlePage = this.props.currentListPage == 0 ? 
                        "Daftar Usulan" : "Semua Usulan";
    return (
        <View style={[styles.navbarWrapper,styles.topNavbar]}>
            <TouchableHighlight onPress={this.props.handleMenuPress}> 
                {
                    this.props.isSearching ?
                    <View style={{width : 0}} /> :
                    <Icon name="navicon" size={28} color="#222"/>
                }
            </TouchableHighlight>
            {
                this.props.isSearching ?
                <SearchBox /> :
                <TitleText titleText="Daftar Usulan" />
            }
            <TouchableHighlight onPress={this.props.handleSearchPress}> 
                <Icon name={searchIcon} size={24} color="#222" />
            </TouchableHighlight>
        </View>
)
}
}

module.exports = Navbar;

