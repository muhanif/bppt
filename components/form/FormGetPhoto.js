import React, { Component } from 'react';
import Icon  from 'react-native-vector-icons/EvilIcons';
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TextInput,
  Picker,
  View
} from 'react-native';
import Camera from 'react-native-camera';
import styles from './form.styles.js';

import {Button} from './index.js';
export default class FormCamera extends Component{
  //TODO : CAMERA ICON NOT SHOWING !
  render(){
    return (
      <View style={styles.container}>
        <Camera 
          ref={(cam) => (this.camera = cam)}
          style={styles.cameraPreview}
          aspect={Camera.constants.Aspect.fill}
        >
          <Text
            style={{flex : 0}}
          > Take photo </Text>
        </Camera>
        <Button 
          style={styles.cameraCapture}
        >
          <View style={styles.cameraIcon}>
            <Icon 
              name="camera" 
              size={20} 
              color={"#000"}
            />
          </View>
        </Button>
      </View>
    )
  }
}
