import React, { Component } from 'react';
import Icon  from 'react-native-vector-icons/EvilIcons';
import {
  View,
  Navigator,
  StyleSheet
} from 'react-native';
import { Button } from './index.js'


export default class FormNavigation extends Component{

  constructor(props){
    super(props)
    this.state = {
      currentIndex : null
    }
  }


  componentDidMount(){
    this.setState({
      currentIndex : 0
    })
  }

  componentDidUpdate(){
  }

  prevButton(){
    const index = this.state.currentIndex
    const routeStack = this.props.routeStack
    this.props.navigator.pop();
    this.setState({
      currentIndex : index - 1,
    })
  }


  nextButton(){
    const index = this.state.currentIndex
    const routeStack = this.props.routeStack
    this.props.navigator.push(routeStack[index+1]);
    this.setState({
      currentIndex : index + 1,
    })
  }

  render(){
    return (
      <View style={NavStyle.container}>
        { this.state.currentIndex !== 0 ?
        <Button onPress={this.prevButton.bind(this)}
          text={"prev"}/> 
          :<View/>
        }
        <Button onPress={this.props.cancelForm}
          text={"cancel"}/> 
      {this.state.currentIndex !== 3?
        <Button text="next" onPress={this.nextButton.bind(this)} />
        :<View/>
      }
        </View>
  )
  }

}

const NavStyle = StyleSheet.create({
  container : {
    flexDirection :'row',
    alignItems : 'center',
    justifyContent : 'space-between',
    position : 'absolute',
    bottom : 2,
    right : 0,
    left : 0,
    marginHorizontal : 10,
  }
})
