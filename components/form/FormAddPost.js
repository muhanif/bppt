
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    TextInput,
    Picker,
    View
} from 'react-native';
import Camera from 'react-native-camera';

import {
    InputGroup,
    PickerGroup,
    Button,
} from './index.js'
import styles from './form.styles.js'

export default class FormAddPost extends Component{
    render(){
        return (
            <View>
            <InputGroup
                titleText='judul'
                text=" "
            />
            <InputGroup
                titleText='deskripsi usulan'
                text=" "
                multiline={true}
                numberOfLines={4}
            />
            <PickerGroup
                picked='Bandung'
                itemList={["Bandung","Jogja","etc"]}
            />
            <PickerGroup
                picked='Kecamatan 1'
                itemList={["Kecamatan 1","Kecamatan 2","Kecamatan 3"]}
            />
            <PickerGroup
                picked='keluarahan 1'
                itemList={["Keluarahan 1","Kelurahan 2","Kelurahan 3"]}
            />
        </View>
        )
    }

}
