import {
    StyleSheet
} from 'react-native'; 
const styles = StyleSheet.create({
    cameraCapture :{
        alignSelf : 'center',
        justifyContent: 'center',
        alignItems: 'center',
        marginVertical : 15,
        width: 60,
        height : 60,
        borderWidth: 10,
        borderColor : "#333344",
        borderRadius: 50,
        backgroundColor: "#444457",
    },
    cameraIcon :{
        //TODO : Icon not show
        alignSelf: 'center',
        position: 'absolute',
        top:0,
        bottom:0,
        left:0,
        right:0,
    },
    cameraPreview : {
        flex : 2,
        justifyContent: 'space-between',
        alignItems: 'center',
        height:300,
       
    },
    inputText : {
        color : '#00f',
        fontSize : 18,
    },
    headerTextWrapper:{
        flex : 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        
    },
    formWrapper:{
        marginHorizontal:4,
    },
    container:{
        flex: 1,
        justifyContent: 'space-between',
        padding:20,
    },
    Button: {
        alignItems:'center',
        flexDirection: 'row',
        justifyContent:'space-around',
        alignSelf : 'center',
        width:70,
        height:32,
        backgroundColor:"#dce0f1"
    },
    buttonText:{
        color : "#fff",
        padding :0,
        margin :0,
        fontSize: 15,
        fontWeight: "200"
    }
})

export default styles;
