// ES 6 import = module / helper
// ES 5 import = Component / Styling (style)

import React, { Component } from 'react';
import Icon  from 'react-native-vector-icons/EvilIcons';
import {
  AppRegistry,
  Navigator,
  Alert,
  ScrollView,
  DrawerLayoutAndroid,
} from 'react-native';

//testing
import Navbar from './components/Navbar';
import Sidemenu from './components/Sidemenu';
import Home from './views/Home.js';
import Proyek from './views/ListProyek.js'; 
//main aplication helper


class bppt extends Component{

  constructor(props){
    super(props);
    this.state = { 
      titleText : "BPPT",
      selectedDrawer : 0, 
    };

    this.routeStack=[
      {
      index : 'Home',
      component : Home,
      passProps : {
        routeStack : this.routeStack
      }
    },
    {
      index : 'Proyek',
      component : Proyek,
      passProps : {
        routeStack : this.routeStack
      }
    },
    ]

  }


  renderScene(route,navigator){
    return React.createElement(route.component, { ...this.props, ...route.passProps, route, navigator } )
    
  }

  handleMenuPress(){
    this.refs['DrawerLayout'].openDrawer();
  }
  linkListener(n){
    this.refs['DrawerLayout'].closeDrawer();
    this.refs['appNav'].resetTo(this.routeStack[n]);
  }
  

  render(){
    return(
      <DrawerLayoutAndroid
        drawerWidth={250}
        drawerPosition={DrawerLayoutAndroid.positions.left}
        renderNavigationView={
          () => (<Sidemenu 
                  linkListener={this.linkListener.bind(this)}
                 />)
        }
        ref={'DrawerLayout'}
      >
      <Navbar 
        titleText={this.state.titleText}
        onPress={this.handleMenuPress.bind(this)}
      />
      <Navigator 
        initialRoute={this.routeStack[0]}
        initialRouteStack={this.routeStack}
        renderScene={this.renderScene}
        ref={'appNav'}
      />
      </DrawerLayoutAndroid>
  )
 }

}            

AppRegistry.registerComponent('bppt', () => bppt);
