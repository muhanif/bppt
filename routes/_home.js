import React from 'react';

const HomeView = {
    ListUsulan : require('../views/index.js'),
    ListProyek : require('../views/ListProyek.js'),
    Search : require('../views/Form.js')
}



module.exports = (viewName) => (HomeView[viewName]);
